import org.bytedeco.javacpp.indexer.IntBufferIndexer;
import org.bytedeco.javacpp.opencv_core;

import static org.bytedeco.javacpp.opencv_core.LINE_AA;
import static org.bytedeco.javacpp.opencv_imgproc.line;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 1/26/2016
 * Time: 11:38 AM
 */
public class LineFinder {

    public static final int    DELTA_RHO   = 1;
    public static final double DELTA_THETA = Math.PI / 180;
    public static final int    MIN_VOTES   = 10;
    public static final int    MIN_LENGTH  = 0;
    public static final int MIN_GAP = 0;

    private final double deltaRho;
    private final double deltaTheta;
    private final int    minVotes;
    private final int    minLength;
    private final int    minGap;

    public LineFinder() {
        this(DELTA_RHO, DELTA_THETA, MIN_VOTES, MIN_LENGTH, MIN_GAP);
    }

    public LineFinder(double deltaRho, double deltaTheta, int minVotes, int minLength, int minGap) {
        this.deltaRho = deltaRho;
        this.deltaTheta = deltaTheta;
        this.minVotes = minVotes;
        this.minLength = minLength;
        this.minGap = minGap;
    }

    public opencv_core.Mat findLines(opencv_core.Mat src) {
        final opencv_core.Mat lines = new opencv_core.Mat();
        org.bytedeco.javacpp.opencv_imgproc.HoughLinesP(src, lines, deltaRho, deltaTheta, minVotes, minLength, minGap);
        return lines;
    }

    public opencv_core.Mat drawDetectedLines(opencv_core.Mat src, opencv_core.Mat lines) {
        final opencv_core.Mat result = new opencv_core.Mat();
        src.copyTo(result);
        IntBufferIndexer indexer = lines.createIndexer();
        for (int i = 0; i < lines.rows(); i++) {
            opencv_core.Point a = new opencv_core.Point(indexer.get(i, 0, 0), indexer.get(i, 0, 1));
            opencv_core.Point b = new opencv_core.Point(indexer.get(i, 0, 2), indexer.get(i, 0, 3));
            line(result, a, b, new opencv_core.Scalar(0, 0, 255, 128), 1, LINE_AA, 0);
        }
        return result;
    }
}
