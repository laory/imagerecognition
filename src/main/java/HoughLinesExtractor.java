import helpers.objects.Line;
import org.bytedeco.javacpp.indexer.FloatBufferIndexer;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Scalar;

import static java.lang.Math.*;
import static org.bytedeco.javacpp.opencv_core.LINE_8;
import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgproc.*;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class HoughLinesExtractor {

    public static final double DISTANCE_RESOLUTION_IN_PX = 0.95;
    public static final double ANGLE_RESOLUTION_IN_RAD   = Math.PI / 180;
    public static final int    MIN_VOTES                 = 100;

    public static void main(String[] args) {
        final Mat src = imread("data/ustupi.jpg", IMREAD_GRAYSCALE);
        Mat houghLines = extractHoughLines(src);
        Basics.display(houghLines, "Hough lines");
    }

    public static Mat extractHoughLines(Mat src) {
        final Mat houghLines = new Mat();

        final int rows = src.rows();
        int cols = src.cols();

        final Mat cannyContours = CannyContoursExtractor.getContours(src);

        final Mat lines = new Mat();
        HoughLines(cannyContours, lines, DISTANCE_RESOLUTION_IN_PX, ANGLE_RESOLUTION_IN_RAD, MIN_VOTES);
        final FloatBufferIndexer indexer = lines.createIndexer();

        src.copyTo(houghLines);
        cvtColor(src, houghLines, COLOR_GRAY2BGR);

        for (int i = 0; i < lines.rows(); i++) {
            final double rho = indexer.get(i, 0, 0);
            final double theta = indexer.get(i, 0, 1);
            final Line builtLine = buildRow(rows, cols, rho, theta);
            line(houghLines, builtLine.a, builtLine.b, new Scalar(0, 0, 255, 0), 1, LINE_8, 0);
        }

        return houghLines;
    }

    private static Line buildRow(int rows, int cols, double rho, double theta) {
        if (theta < Math.PI / 4.0 || theta > 3.0 * Math.PI / 4.0) {
            // vertical line
            // point of intersection of the line with first row
            final Point a = new Point(Long.valueOf(round(rho / cos(theta))).intValue(), 0);
            //point of intersection of the line with last row
            final Point b = new Point(Long.valueOf(round((rho - rows * sin(theta)) / cos(theta))).intValue(), rows);
            return new Line(a, b);
        } else {
            // horizontal line
            // point of intersection of the line with first column
            final Point a = new Point(0, Long.valueOf(round(rho / sin(theta))).intValue());
            //point of intersection of the line with last column
            final Point b = new Point(cols, Long.valueOf(round((rho - cols * cos(theta)) / sin(theta))).intValue());
            return new Line(a, b);
        }
    }
}
