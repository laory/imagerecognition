import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.OpenCVFrameConverter;

import javax.swing.*;

import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgproc.Laplacian;
import static org.bytedeco.javacpp.opencv_core.BORDER_DEFAULT;

/**
 * Created by Hedgehog on 23/1/16.
 * Open image and display it as it is and after Laplacian conversion.
 */
public class Basics {

    public static void main(String[] args) {
        final Mat src = imread("data/boldt.jpg");
        display(src, "Input");

        final Mat dest = new Mat();
        Laplacian(src, dest, src.depth(), 1, 3, 0, BORDER_DEFAULT);
        display(dest, "Laplacian");
    }

    public static void display(Mat image, String caption) {
        final CanvasFrame canvasFrame = new CanvasFrame(caption, 1.0);
        final OpenCVFrameConverter converter = new OpenCVFrameConverter.ToMat();

        canvasFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        canvasFrame.showImage(converter.convert(image));
    }
}
