package helpers.objects;

import org.bytedeco.javacpp.opencv_core.Point;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class Line {

    public final Point a;
    public final Point b;

    public Line(Point a, Point b) {
        this.a = a;
        this.b = b;
    }
}
