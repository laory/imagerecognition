import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;

import static org.bytedeco.javacpp.opencv_imgcodecs.IMREAD_GRAYSCALE;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgproc.COLOR_GRAY2BGR;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 1/26/2016
 * Time: 11:16 AM
 */
public class HoughLineSegments {

    public static final int THRESHOLD_1   = 125;
    public static final int THRESHOLD_2   = 350;
    public static final int APERTURE_SIZE = 3;

    public static void main(String[] args) {
        final opencv_core.Mat src = imread("data/stop.jpg", IMREAD_GRAYSCALE);
        final Mat houghLineSegments = findHoughLineSegments(src);
        Basics.display(houghLineSegments, "Hough line segments");
    }

    private static opencv_core.Mat findHoughLineSegments(opencv_core.Mat src) {
        final opencv_core.Mat contours =
                CannyContoursExtractor.getContours(src, THRESHOLD_1, THRESHOLD_2, APERTURE_SIZE, false);

        final LineFinder finder = new LineFinder(LineFinder.DELTA_RHO, LineFinder.DELTA_THETA, 80, 100, 20);
        final Mat lines = finder.findLines(contours);

        final opencv_core.Mat layout = new opencv_core.Mat();
        cvtColor(contours, layout, COLOR_GRAY2BGR);
        return finder.drawDetectedLines(layout, lines);
    }
}
