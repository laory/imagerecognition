import org.bytedeco.javacpp.opencv_core.Mat;

import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgproc.Canny;

/**
 * Created by Hedgehog on 23/1/16.
 */
public class CannyContoursExtractor {

    public static final int THRESHOLD_1 = 300;
    public static final int THRESHOLD_2 = 400;
    public static final int APERTURE_SIZE = 3;

    public static void main(String[] args) {
        final Mat src = imread("data/ustupi.jpg");
        final Mat contours = getContours(src);
        Basics.display(contours, String.format("Canny Contours. Aperture size: %d, threshold1: %d, threshold2: %d", APERTURE_SIZE, THRESHOLD_1, THRESHOLD_2));
    }

    public static Mat getContours(Mat src) {
        final Mat contours = new Mat();
        Canny(src, contours, THRESHOLD_1, THRESHOLD_2, APERTURE_SIZE, true);
        return contours;
    }

    public static Mat getContours(Mat src, int threshold1, int threshold2, int apertureSize, boolean l2Gradient) {
        final Mat contours = new Mat();
        Canny(src, contours, threshold1, threshold2, apertureSize, l2Gradient);
        return contours;
    }
}
